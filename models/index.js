if (!global.hasOwnProperty('db')) {
  var Sequelize = require('sequelize')
    , sequelize = null

  sequelize = new Sequelize(process.env.DATABASE_URL, {
    dialect:  'postgres',
    protocol: 'postgres',
    logging:  true,
    dialectOptions: {
      ssl: true
    }
  });

  global.db = {
    Sequelize: Sequelize,
    sequelize: sequelize,
    User:      sequelize.import(__dirname + '/user'),
    Game:      sequelize.import(__dirname + '/game')
  }
}

global.db.Game.belongsTo(global.db.User, {
    foreignKey: 'user_id'
});

global.db.Game.belongsTo(global.db.User, {
    foreignKey: 'breaker_id',
    as: 'Breaker'
});

module.exports = global.db