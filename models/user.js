var crypto = require('crypto');

module.exports = function(sequelize, DataTypes) {
  return sequelize.define("User", {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      isEmail: true
    },
    token: DataTypes.STRING
  }, {
    hooks: {
      afterValidate: function(user, options) {
        var sha1 = crypto.createHash('sha1');
        var salt = Math.floor(new Date() / 1000);
        sha1.update(user.name + salt);
      
        user.token = sha1.digest('hex');
      }
    }
  });
}
