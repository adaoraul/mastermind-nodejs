module.exports = function(sequelize, DataTypes) {
  return sequelize.define("Game", {
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    breaker_id: {
      type: DataTypes.INTEGER
    },
    colors: {
      type: DataTypes.ARRAY(DataTypes.CHAR),
      allowNull: false
    },
    played: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    startedAt: {
      type: DataTypes.DATE
    },
    finishedAt: {
      type: DataTypes.DATE
    }
  });
}
