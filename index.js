require('app-module-path').addPath(__dirname + '/models');

var express = require('express');
var gravatar = require('gravatar');
var bodyparser = require('body-parser');
var db = require('./models');

var app = express();

// setting up port for heroku
app.set('port', (process.env.PORT || 5000));

// json parsing
app.use(bodyparser.json());

// root_url
app.get('/', function(request, response) {
  response.json({ status: true });
});

// login
app.post('/login', function(request, response, next) {
  db.User.create({ name: request.body.name, email: request.body.email })
  .then(function (user) {
    response.json({status: true, avatar: gravatar.url(user.email), token: user.token});
  })
  .catch(Error, function(error) {
    response.status(400);
    response.json({status: false, error: error});
  });  
});

// games
// listing
app.get('/games', function(request, response, next) {
  var listedGames = [];
  db.Game.findAll({
    include: [
      { model: db.User }
    ]
  })
  .then(function(games) {
    games.forEach(function(game) {
      listedGames.push({
        codemaker: {
          name: game.User.name
        },
        game_id: game.id,
        played: game.played
      });
    });
    
    response.json({ list: listedGames });
  });
});

// creating
app.post('/games', function(request, response, next) {
  db.User.findOne({ where: { token: request.body.token }})
  .then(function(user) {
    if (user) {
      db.Game.create({ user_id: user.id, colors: request.body.colors })
      .then(function (game) {
        response.json({status: true, game_id: game.id});
      })
      .catch(Error, function(error) {
        response.status(400);
        response.json({status: false, error: error});
      });
    } else {
      response.status(400);
      response.json({status: false, error: user});
    }
  });
});

//starting game
app.post('/games/:id/play', function(request, response, next) {
  db.User.findOne({ where: { token: request.body.token }})
  .then(function(user) {
    if (user) {
      db.Game.findOne({ where: { id: request.params.id }, include: [{ model: db.User }]})
      .then(function(game) {
        if (game) {
          if (game.User.id == user.id) {
            response.status(400);
            response.json({status: false, error: 'You cannot play your own game'});
          } else {
            db.Game.update({ breaker_id: user.id, startedAt: global.db.sequelize.fn('NOW') }, { where: { id: game.id }})
            .then(function (game) {
              response.json({status: true, time_limit: 300});
            }).catch(Error, function(error) {
              response.status(400);
              response.json({status: false, error: error});
            }); 
          }
        } else {
          response.status(400);
          response.json({status: false, error: game});
        }
      });
    } else {
      response.status(400);
      response.json({status: false, error: user});
    }
  });
});

// hack for angularjs crossdomain bug
app.all('*', function(request, response, next) {
    response.header('Access-Control-Allow-Origin', request.headers.origin);
    response.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    response.header('Access-Control-Allow-Credentials', false);
    response.header('Access-Control-Max-Age', '86400');
    response.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    next();
});

app.options('*', function(req, res) {
    res.send(200);
});

// app
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
