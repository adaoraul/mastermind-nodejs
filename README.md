# NOT FINISHED
## See https://github.com/adaoraul/mastermind

# Mastermind Challenge

### Create new player

* Endpoint: http://api-dreamteam-axiomzen.herokuapp.com/login
* Method: [POST]

_params:_
```json
{
  "name": "Adão Raul",
  "email": "adao.raul@gmail.com"
}
```

_response:_
```json
{
  "status": true,
  "token": "ff62afc1d35e0c479a4897e704f8046df4ebe8c8b8182fb7"
}
```

----------

### Create new game

* Endpoint: http://api-dreamteam-axiomzen.herokuapp.com/games
* Method: [POST]

_params:_

```json
{
  "token": "ff62afc1d35e0c479a4897e704f8046df4ebe8c8b8182fb7",
  "colors": "RPYGOGOP"
}
```
_response:_

```json
{
  "status": true,
  "game_code": "aa7b1ed50824d64a97c8d7ae"
}
```

----------

### Get all games

* Endpoint: http://api-dreamteam-axiomzen.herokuapp.com/games
* Method: [GET]

_params:_
```json
{
  "token": "ff62afc1d35e0c479a4897e704f8046df4ebe8c8b8182fb7"
}
```
_response:_
```json
{
  "status": true,
  "list": [
    {
      "codemaker": {
        "name": "André Souza",
      },
      "game_code": "aa7b1ed50824d64a97c8d7ae",
      "played": false
    },
    {
      "codemaker": {
        "name": "Adão Raul",
      },
      "game_code": "e0563887739eb358202bf912",
      "played": false
    }
  ]
}
```

----------

## On games list

```ruby
if played == true
```

### Get game description
* Endpoint: http://api-dreamteam-axiomzen.herokuapp.com/games/:id
* Method: [GET]

_params:_
```json
{
  "token": "ff62afc1d35e0c479a4897e704f8046df4ebe8c8b8182fb7"
}
```
_response:_
```json
{
  "status": true,
  "codemaker": {
    "name": "André Souza",
  },
  "codebreaker": {
    "name": "Adão Raul",
  },
  "colors": "RPYGOGOP",
  "solved": true,
  "time_taken": 64.75358,
  "turns": [
    {
      "guess": "BRYDYRND",
      "exact": 3,
      "near": 2
    },
    {
      "guess": "BRYDNWND",
      "exact": 2,
      "near": 1
    },
    {
      "guess": "BRYDYRND",
      "exact": 3,
      "near": 2
    }
  ]
}
```

----------

```ruby
if played == false
```

### Start playing some created game

* Endpoint: http://api-dreamteam-axiomzen.herokuapp.com/games/:id/play
* Method: [GET]

_params:_
```json
{
  "token": "ff62afc1d35e0c479a4897e704f8046df4ebe8c8b8182fb7"
}
```

```json
_response:_
{
  "status": true,
  "colors": "RBGYOPCM",
  "code_length": 8,
  "time_limit": 300
}
```

----------

### Send guess for a game you are playing

* Endpoint: http://api-dreamteam-axiomzen.herokuapp.com/games/:id/guess
* Method: [POST]

params
```json
{
  "token": "ff62afc1d35e0c479a4897e704f8046df4ebe8c8b8182fb7",
  "guess": "BRYDYRND"
}
```

response
```json
{
  "status": true,
  "exact": 3,
  "near": 2,
  "solved": false,
  "turns": 1,
  "time_limit": 256
}
```

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku Toolbelt](https://toolbelt.heroku.com/) installed.

```sh
$ git clone git@github.com:heroku/node-js-getting-started.git # or clone your own fork
$ cd node-js-getting-started
$ npm install
$ npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)